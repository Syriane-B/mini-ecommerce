# Mini site de E-commerce

## Back
Back is made with Strapi.
``` 
cd back
npm install
npm run develop
```
Api: http://localhost:1337
Admin: http://localhost:1337/admin

## Front
Front is made with Angular, with Prime ng library

```
cd front
npm install
npx ng serve
```
App run here: http://localhost:4200

To test the app run 
```
npx ng test
```
It will run all test defined in the app, in Karma server with Jasmine test framework.
product card component is tested
