import {Category} from "./category";
import {Image} from "./image";

export type Product = {
  name: string,
  pictures: Image[],
  categories: Category[],
  date : string,
  description: string,
  baseLine: string,
}
