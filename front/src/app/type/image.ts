export type Image = {
  url: string,
  name: string,
  formats: {
    small : {
      url: string;
    }
  }
}
