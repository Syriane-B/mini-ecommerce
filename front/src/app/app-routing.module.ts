import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./page/home/home.component";
import {LoginComponent} from "./page/login/login.component";
import {AccountComponent} from "./page/account/account.component";
import {ProductsComponent} from "./page/products/products.component";
import {ProductComponent} from "./page/product/product.component";
import {CartComponent} from "./page/cart/cart.component";
import {RegisterComponent} from "./page/register/register.component";

const routes: Routes = [
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'my-account', component: AccountComponent },
  { path: 'cart', component: CartComponent },
  { path: 'contact', component: CartComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'product/:id', component: ProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
