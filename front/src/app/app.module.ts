import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';

import {InputSwitchModule} from 'primeng/inputswitch';
import {ButtonModule} from 'primeng/button';
import {MenubarModule} from 'primeng/menubar';
import {CheckboxModule} from 'primeng/checkbox';
import {InputTextModule} from 'primeng/inputtext';
import { SwiperModule } from 'swiper/angular';

import { NavComponent } from './ui/nav/nav.component';
import { ProductsComponent } from './page/products/products.component';
import { AccountComponent } from './page/account/account.component';
import { LoginComponent } from './page/login/login.component';
import { ProductComponent } from './page/product/product.component';
import { CartComponent } from './page/cart/cart.component';
import {RippleModule} from "primeng/ripple";
import { ProductCardComponent } from './ui/product-card/product-card.component';
import { RegisterComponent } from './page/register/register.component';
import { ContactComponent } from './page/contact/contact.component';
import {FormsModule} from "@angular/forms";
import {CardModule} from "primeng/card";


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    ProductsComponent,
    AccountComponent,
    LoginComponent,
    ProductComponent,
    CartComponent,
    ProductCardComponent,
    RegisterComponent,
    ContactComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        InputSwitchModule,
        MenubarModule,
        CheckboxModule,
        InputTextModule,
        ButtonModule,
        SwiperModule,
        RippleModule,
        FormsModule,
        CardModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
