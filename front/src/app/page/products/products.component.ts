import { Component, OnInit } from '@angular/core';
import {StoreService} from "../../services/store.service";
import {Product} from "../../type/product";
import {Category} from "../../type/category";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];
  categories: Category[] = [];
  selectedCategoryIds: number[] = [];

  constructor(
    private storeService: StoreService
  ) { }

  ngOnInit(): void {
    this.storeService.getProducts().subscribe(
      (result) => {
        this.products = result;
      },
      (error) => {
      }
    );
    this.storeService.getCategories().subscribe(
      (result) => {
        this.categories = result;
      },
      (error) => {
      }
    );
  }
  handleTagClick(clickedCategoryId: number) {
    console.log(this.products);
    let newSelectedCategoryIds = this.selectedCategoryIds;
    const indexOfClickedTag = newSelectedCategoryIds.indexOf(clickedCategoryId);
    // if the tag is not selected yet
    if (indexOfClickedTag === -1) {
      // if the new selected tag is all, remove all other tags
      newSelectedCategoryIds.push(clickedCategoryId);
    } else {
      // else remove the tag from the list
      newSelectedCategoryIds.splice(indexOfClickedTag, 1);
    }
    this.selectedCategoryIds = newSelectedCategoryIds;
  }

  isTagSelected(categoryId: Number) {
    let isSelected = false;
    if (this.selectedCategoryIds.length > 0) {
      this.selectedCategoryIds.forEach( selectedId => {
        if (categoryId === selectedId) {
          isSelected = true;
        }
      });
    }
    return isSelected;
  }

  showItem(categories: Category[]) {
    let isSelected = false;
    if (this.selectedCategoryIds.length === 0) {
      return true;
    } else {
      this.selectedCategoryIds.forEach( value => {
        categories.forEach(category => {
          if (category.id === value) {
            isSelected = true;
          }
        });
      });
    }
    return isSelected;
  }
  handleAllClick() {
    if (this.selectedCategoryIds.length !== 0) {
      this.selectedCategoryIds = [];
    }
  }
}

