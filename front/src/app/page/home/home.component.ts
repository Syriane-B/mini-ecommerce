import {Component, ViewEncapsulation, ViewChild, OnInit} from "@angular/core";
import { SwiperComponent } from "swiper/angular";

// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  screens: any[] = [];

  constructor() { }

  ngOnInit(): void {
    this.screens = [
      {
        pictureName: 'home1.jpg',
        title: 'Bienvenue sur notre shop',
        baseline: 'retrouvez tous nos produits disponibles',
        linkText: 'Découvrez nos produits',
        link: '/products'
      },
      {
        pictureName: 'home2.jpg',
        title: 'Title 2',
        baseline: 'Text2',
        linkText: '',
        link: ''
      },
      {
        pictureName: 'home3.jpg',
        title: 'Title 3',
        baseline: 'Text3',
        linkText: '',
        link: ''
      }
    ]
  }

}
