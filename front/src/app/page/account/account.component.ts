import { Component, OnInit } from '@angular/core';
import {StoreService} from "../../services/store.service";
import {Router} from "@angular/router";
import {LocalStorageService} from "angular-web-storage";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(
    private storeService: StoreService,
    private router: Router,
    private storage: LocalStorageService
  ) { }

  ngOnInit(): void {
    if (!this.storeService.isLoggedIn()) {
      this.router.navigate(['/login']);
    } else {
      let userId = this.storage.get('userId');
      this.storeService.getUserInfos(userId).subscribe(
        (result) => {
          console.log(result);
        },
        (error) => {
        }
      )
    }
  }

}
