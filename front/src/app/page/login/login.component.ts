import { Component, OnInit } from '@angular/core';
import {NetworkService} from "../../services/network.service";
import {Observable} from "rxjs";
import {environment} from "../../env/environment";
import {catchError, map, tap} from "rxjs/operators";
import { LocalStorageService } from 'angular-web-storage';
import {StoreService} from "../../services/store.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userCredential: {identifier: string, password: string} = {identifier: '', password: ''};
  constructor(
    private networkService: NetworkService,
    private storage: LocalStorageService,
    private storeService: StoreService,
    private router: Router
    ) { }

  ngOnInit(): void {
  }

  logUser(): any | void  {
    if (this.userCredential.identifier === '' || this.userCredential.password === '') {
      return { error: 'no credentials' };
    } else {
      this.networkService.post(environment.apiUrl + '/auth/local', this.userCredential).pipe(
        tap(result => {
          console.log(result);
          this.storage.clear();
          this.storage.set('jwtToken', result.jwt);
          this.storage.set('userId', result.user.id);
        }),
        catchError((err) => {
          // TODO catch error
          return [];
        })
      ).subscribe(() => {
        if (this.storeService.isLoggedIn()) {
          this.router.navigate(['/my-account'])
        }
      });
    }
  }
}
