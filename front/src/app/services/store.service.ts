import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import {Observable} from "rxjs";
import {environment} from "../env/environment";
import {catchError, map} from "rxjs/operators";
import {LocalStorageService} from "angular-web-storage";

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(
    private networkService: NetworkService,
    private storage: LocalStorageService
  ) { }

  getProducts(): Observable<any> {
    return this.networkService
      .get(environment.apiUrl + '/products')
      .pipe(
        map(result => {
          if (result.length !== 0) {
            return result;
          } else {
            return [];
          }
        }),
        catchError((err, caught) => {
          // TODO catch the error
          return [];
        })
      );
  }

  getCategories(): Observable<any> {
    return this.networkService
      .get(environment.apiUrl + '/categories')
      .pipe(
        map(result => {
          if (result.length !== 0) {
            return result;
          } else {
            return [];
          }
        }),
        catchError((err, caught) => {
          // TODO catch the error
          return [];
        })
      );
  }

  getUserInfos(userId: number): Observable<any> {
    //TODO get user infos
    return this.networkService
      .get(environment.apiUrl + '/products')
      .pipe(
        map(result => {
          if (result.length !== 0) {
            return result;
          } else {
            return [];
          }
        }),
        catchError((err, caught) => {
          // TODO catch the error
          return [];
        })
      );
  }

  isLoggedIn(): boolean {
    return !(this.storage.get('jwtToken') === null);
  }

}
