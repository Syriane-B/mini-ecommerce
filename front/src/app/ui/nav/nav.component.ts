import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  items: MenuItem[] =  [];

  constructor() { }

  ngOnInit(): void {
    this.items =  [
      {
        label:'Home',
        icon:'pi pi-home',
        routerLink: ['home']
      },
      {
        label:'My account',
        icon:'pi pi-user',
        routerLink: ['my-account']
      },
      {
        label:'Login',
        icon:'pi pi-sign-in',
        routerLink: ['login']
      },
      {
        label:'Shop',
        icon:'pi pi-tags',
        routerLink: ['products']
      }
    ];
  }

}
