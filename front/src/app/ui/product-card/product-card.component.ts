import { Component,Input, OnInit } from '@angular/core';
import {environment} from "../../env/environment";

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product: any;
  imagesBaseUrl: string = environment.apiUrl;

  constructor() { }

  ngOnInit(): void {
    console.log(this.product.pictures[0])
  }

}
