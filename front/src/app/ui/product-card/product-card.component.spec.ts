import { async, ComponentFixture, TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
import { DebugElement } from "@angular/core";
import { By } from '@angular/platform-browser';
import { ProductCardComponent } from './product-card.component';
import {Product} from "../../type/product";
import {Image} from "../../type/image";
import {Category} from "../../type/category";

describe('ProductCardComponent', () => {
  let component: ProductCardComponent;
  let fixture: ComponentFixture<ProductCardComponent>;
  let de: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    // mock the hero supplied by the parent component
    let expectedProduct: Product = {
      name: 'nom du produit',
      categories: [
        {
          id: 1,
          name: 'Categorie 1'
        }
      ],
      date: '2021-09-28',
      pictures: [
        {
          url: '',
          name: 'nom du l images',
          formats: {
            small: {
              url: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.thepumphouse.org%2Fproduct%2Fopen-clay-studio-1%2F&psig=AOvVaw1jX2V0Jt6XR2AdLxKRy8JF&ust=1633078786721000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCLC6p9uqpvMCFQAAAAAdAAAAABAD'
            }
          }
        }
      ],
      description: 'Description du produit',
      baseLine: 'baseline du produit',
    };

    // simulate the parent setting the input property with that hero
    component.product = expectedProduct;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should display product name `nom du produit`', () => {
    expect(fixture.nativeElement.textContent).toContain('nom du produit');
  })
});
